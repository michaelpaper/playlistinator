#!/bin/python3

import pycurl
import certifi
from io import BytesIO
from urllib.parse import quote
from urllib.parse import urlencode
import json
import sys
import time
import os


def make_http_request(url, headers, data):
    buffer = BytesIO()
    c = pycurl.Curl()
    c.setopt(c.WRITEDATA, buffer)
    c.setopt(c.CAINFO, certifi.where())
    c.setopt(c.HTTPHEADER, headers)
    c.setopt(c.URL, url)
    c.setopt(c.HTTP_VERSION, c.CURL_HTTP_VERSION_1_1)
    if data:
        c.setopt(c.POSTFIELDS, data.encode('utf-8'))
    c.perform()
    c.close()
    body = buffer.getvalue()
    return body.decode('UTF-8')


def make_get_request(url, headers):
    return make_http_request(url, headers, None)


def make_post_request(url, headers, data):
    return make_http_request(url, headers, data)


def get_apikey_setlist():
    apikey_fd = open(os.path.dirname(__file__) +  "/apikey_setlist.txt")
    apikey = apikey_fd.read()
    apikey_fd.close()
    return apikey


def get_user_id(apikey):
    url = "https://api.spotify.com/v1/me"
    headers = ['Accept: application/json',
               'Content-Type: application/json',
               'Authorization: Bearer ' + apikey]
    request = make_get_request(url, headers)
    user_id = json.loads(request)["id"]
    return user_id


def create_playlist(apikey, user_id, title, description):
    url = "https://api.spotify.com/v1/users/"
    url += quote(user_id)
    url += "/playlists"
    data = '{"name":"' + title + '","description":"' + description + \
            '","public":false}'
    headers = ['Accept: application/json',
               'Content-Type: application/json',
               "Content-Length: " + str(len(data.encode('utf-8')) + 2),
               'Authorization: Bearer ' + apikey]
    request = make_post_request(url, headers, data)
    playlist_id = json.loads(request)["id"]
    print(playlist_id)
    return playlist_id


def geturi(apikey, track, artist):
    url = "https://api.spotify.com/v1/search?q=track%3A%20"
    url += quote(track)
    url += "%20artist%3A%20"
    url += quote(artist)
    url += "&type=track"
    headers = ['Accept: application/json',
               'Content-Type: application/json',
               'Authorization: Bearer ' + apikey]
    request = make_get_request(url, headers)
    uri = json.loads(request)["tracks"]["items"][0]["uri"]
    return uri


def request_setlistfm(apikey, url):
    headers = ['Accept: application/json',
               'x-api-key: ' + apikey]
    return make_get_request(url, headers)


def get_artists_list(apikey, artist_name):
    url = 'https://api.setlist.fm/rest/1.0/search/artists?artistName='
    url += quote(artist_name)
    request = request_setlistfm(apikey, url)
    artists = json.loads(request)["artist"]
    print(json.dumps(artists))
    #for artist in artists:
        #print(artist["name"] + ": " + artist["disambiguation"])


def get_artist(apikey, artist_name, hint):
    url = 'https://api.setlist.fm/rest/1.0/search/artists?artistName='
    url += quote(artist_name)
    request = request_setlistfm(apikey, url)
    artists = json.loads(request)["artist"]
    for artist in artists:
        print(artist["name"] + ": " + artist["disambiguation"])
        if artist_name == artist["name"]:
            if hint is None or hint in artist["disambiguation"].split():
                return artist
    return artists[0]


def get_setlists(apikey, mbid):
    url = 'https://api.setlist.fm/rest/1.0/artist/'
    url += mbid
    url += '/setlists'
    return json.loads(request_setlistfm(apikey, url))


def add_songs_to_playlist(apikey, playlist_id, uris):
    url = "https://api.spotify.com/v1/playlists/"
    url += playlist_id
    url += "/tracks"
    data = '{"uris":' + json.dumps(uris) + '}'
    headers = ['Accept: application/json',
               'Content-Type: application/json',
               "Content-Length: " + str(len(data.encode('utf-8')) + 2),
               'Authorization: Bearer ' + apikey]
    make_post_request(url, headers, data)


# The first arg should be a setlists.fm API key
def get_artist_setlists(apikey, artist_name, hint):
    artist_json = get_artist(apikey, artist_name, hint)
    best_match_name = artist_json["name"]
    artist = best_match_name
    best_match_mbid = artist_json["mbid"]
    print(artist_json)
    print("best match: {} mbid: {}".format(best_match_name, best_match_mbid))
    # To avoid receiving an error from setlist.fm
    time.sleep(2)
    setlists = get_setlists(apikey, best_match_mbid)
    songs = set()
    if "code" in setlists:
        print(setlists)
        print("Fetching setlists from this artist returned code {}".format(
            setlists["code"]))
        print("There may be an ambiguity, try to add a hint to your command")
        print_syntax_exit()
    for setlist in setlists["setlist"]:
        for songlist in setlist["sets"]["set"]:
            for song in songlist["song"]:
                songs.add(song["name"])
    return songs


# The first arg should be a setlists.fm API key
def get_mbid_setlists(apikey, mbid):
    # To avoid receiving an error from setlist.fm
    time.sleep(2)
    setlists = get_setlists(apikey, mbid)
    songs = set()
    if "code" in setlists:
        print(setlists)
        print("Fetching setlists from this artist returned code {}".format(
            setlists["code"]))
        print_syntax_exit()
    for setlist in setlists["setlist"]:
        for songlist in setlist["sets"]["set"]:
            for song in songlist["song"]:
                songs.add(song["name"])
    return songs


# The first arg should be a Spotify API key
def generate_playlist(apikey, songs, artist_name):
    uris = list(set([geturi(apikey, song, artist_name) for song in songs]))
    user_id = get_user_id(apikey)
    playlist_title = "Setlist " + artist_name
    desc = "All of the songs recently played live by " + artist_name
    playlist_id = create_playlist(apikey, user_id, playlist_title, desc)
    add_songs_to_playlist(apikey, playlist_id, uris)


def print_syntax_exit():
    print("usage: {} <spotify_key> <task> <band> [mbid]".format(sys.argv[0]))
    exit(1)


def main():
    if len(sys.argv) < 4:
        print_syntax_exit()
    apikey_spotify = sys.argv[1] + "\n"
    task = sys.argv[2]
    artist_name = sys.argv[3]
    apikey_setlist = get_apikey_setlist()
    if task == "get_artists" and len(sys.argv) == 4:
        get_artists_list(apikey_setlist, artist_name)
    elif task == "create_playlist" and len(sys.argv) == 5:
        mbid = sys.argv[4]
        setlists = get_mbid_setlists(apikey_setlist, mbid)
        generate_playlist(apikey_spotify, setlists, artist_name)
    else:
        print_syntax_exit()


if __name__ == "__main__":
    main()
