# Playlistinator

A webapp that lets a user log into Spotify, type an artist's name, and
generates a playlist with the songs that this artist played live recently.

To run it, just run

```
node app.js
```

And go to [http://localhost:3001](https://localhost:3001) on your browser.
