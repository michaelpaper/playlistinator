/**
 * This is an example of a basic node.js script that performs
 * the Implicit Grant oAuth2 flow to authenticate against
 * the Spotify Accounts.
 *
 * For more information, read
 * https://developer.spotify.com/web-api/authorization-guide/#implicit_grant_flow
 */

const { exec, spawn } = require('child_process');

var express = require('express'); // Express web server framework
var app = express();

var shellescape = require('shell-escape');



app.get('/generate', function(req, res) {
  var access_token = req.query.access_token;
  var artist_name = req.query.artist_name;
  var artist_mbid = req.query.artist_mbid;

  var args = ["python3", __dirname + "/playlist_creation.py", access_token, "create_playlist", artist_name, artist_mbid];

  var command = shellescape(args);

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.log(error);
      res.send("failure");
      return;
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
      return;
    }
    res.send(`${stdout}`);
  });

});


app.get('/get_artists', function(req, res) {
  var access_token = req.query.access_token;
  var artist_name = req.query.artist_name;

  var args = ["python3", __dirname + "/playlist_creation.py", access_token, "get_artists", artist_name];

  var command = shellescape(args);

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.log(error);
      res.send("failure");
      return;
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
      return;
    }
    res.send(`${stdout}`);
  });

});

app.use(express.static(__dirname + '/public'));
console.log('Listening on 3001');
app.listen(3001);

